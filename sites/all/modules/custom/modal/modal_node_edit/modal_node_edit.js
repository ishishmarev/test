(function($) {
  Drupal.ajax.prototype.commands.views_refresh = function(ajax, response, status) {
    var views = Drupal.settings.views.ajaxViews;
    var dom_id = views[Object.keys(views)[0]]['view_dom_id'];
    var selector = '.view-dom-id-' + dom_id;
    $(selector).triggerHandler('RefreshView');
  }
}(jQuery));